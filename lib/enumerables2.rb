 require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |str|
    return false if !str.include?(substring)
  end
  true
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  unique_arr = []
  non_uniq = []
  string.each_char do |letter|
    if unique_arr.include?(letter)
      non_uniq << letter unless non_uniq.include?(letter)
    end
    unique_arr << letter
  end
  non_uniq.delete(' ')
  non_uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  longest = ''
  longest_two = []
  arr = string.split
  arr.each do |word|
    longest = word if word.size > longest.size
  end
  longest_two << longest
  arr.delete(longest)
  longest = ''
  arr.each do |word|
    longest = word if word.size > longest.size
  end
  longest_two << longest

  longest_two.sort_by { |w| w.size }
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  abc = ('a'..'z').to_a
  abc - string.split(//)
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  good_years = []
  i = first_yr
  while i <= last_yr
    good_years << i if not_repeat_year?(i)
    i += 1
  end
  good_years
end

def not_repeat_year?(year)
  yr = year.to_s.split(//)
  return true if yr.uniq.length == yr.length
  false
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
require 'byebug'

def one_week_wonders(songs)
  wonders = []
  songs.each do |song|
    wonders << song if no_repeats?(song, songs)
  end
  wonders.uniq.sort
end

def no_repeats?(song_name, songs)
  i = 1
  while i < songs.length - 1
    if songs[i] == song_name
      return false if songs[i+1] == song_name || songs[i-1] == song_name
    end
    i += 1
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  closest = ''
  closest_num = 10
  string.delete!('.,?!/:;')
  string.split.each do |word|
    next if !word.include?('c')
    if c_distance(word) < closest_num
      closest = word
      closest_num = c_distance(word)
    end
  end
  closest
end

def c_distance(word)
  i = -1
  while i.abs < word.length
    return i.abs if word[i] == 'c'
    i += 1
  end
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  nested = []
  i = 0
  while i < arr.length - 1
    # debugger
    if arr[i] == arr[i+1]
      x = i + 1
      while arr[i] == arr[x]
        # debugger
        x += 1
      end
      nested << [i, x - 1]
      i = x - 1
    end
    i += 1
  end
  nested
end
